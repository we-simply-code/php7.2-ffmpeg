FROM php:7.2

RUN apt-get update && apt-get install -y \
    ffmpeg \
    git \
    unzip

RUN docker-php-ext-install \
    pdo_mysql \
    bcmath

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer